# Lista de contactos
This project : Lista de contacto , is a page that let you guard edit and see your contacts to see them anytime you want.
It was designed and created by :
                * AJAMI mohamed yassine
                * ALVUJAR Jose 
                * Vladislav KOMARENKO 
                
Frameworks used : 
                  front -> Vue.js
                  back  -> Laravel
data base used :  SQLite



The tools used in this project are : 
     - Code visual studio : writing and executing code
     - Scrum : project management
     - Gitlab
     - Slack : sharing ideas and documentation.
     
The code using in programation respect certain rules :
* El uso del TDD is obligatory.
*those naming methods should be respected :
    -PascalCase -----> Components
    -kebab-case -----> events , CSS(classes and ID)
    -UPPER_CASE -----> CONSTANTES : names that means the same every time , like (API _ KEY)
    -camelCase  -----> the rest , functions etc
* Use Git for version history. 
* No PUSH without validation of another member.
* Use easily pronounceable names for variables and methods.
* With variables use the name to show intention. 
* Be consistent. 
* Don’t hesitate to use technical terms in names. 
* Use a verb as the first word in method and use a noun for class. Use camelCase for variable and function name. The class should start from the capital.
* Make functions apparent. Keep a function as short as possible.
* Don’t comment on bad code. CHANGE IT



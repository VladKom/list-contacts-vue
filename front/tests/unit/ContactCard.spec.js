import { shallowMount } from "@vue/test-utils";
import ContactCard from "@/components/ContactCard.vue";

test("pinta en la pantalla", () => {
  const wrapper = shallowMount(ContactCard, {
    propsData: {
      details: {
        id: "01",
        name: "yassin",
        familyName: "ajami"
      }
    }
  });

  expect(wrapper.text()).toContain("ajami");
  expect(wrapper.text()).toContain("yassin");
});

test("emite event delete click", async () => {
  const contact = {
    id: "01"
  };
  const wrapper = shallowMount(ContactCard, {
    propsData: { details: contact }
  });

  expect(wrapper.emitted().click).toBe(undefined);

  const deletbutton = wrapper.find(".delete");
  deletbutton.trigger("click");
  await wrapper.vm.$nextTick();

  expect(wrapper.emitted()["delete-contact-event"].length).toBe(1);
  expect(wrapper.emitted()["delete-contact-event"][0]).toEqual(["01"]);
});
test("emite event seemore click", async () => {
  const contact = {
    id: "01"
  };
  const wrapper = shallowMount(ContactCard, {
    propsData: { details: contact }
  });

  expect(wrapper.emitted().click).toBe(undefined);

  const seemorebutton = wrapper.find(".details");
  seemorebutton.trigger("click");
  await wrapper.vm.$nextTick();

  expect(wrapper.emitted()["see-more-details-event"].length).toBe(1);
  expect(wrapper.emitted()["see-more-details-event"][0]).toEqual(["01"]);
});

test("Crear un Computed para fullName", async () => {
  const wrapper = shallowMount(ContactCard, {
    propsData: {
      details: {
        id: "01",
        name: "yassin",
        familyName: "ajami"
      }
    }
  });
  await wrapper.vm.$nextTick();
  expect(wrapper.vm.fullName).toContain("yassin ajami");
});

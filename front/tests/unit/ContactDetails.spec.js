import { shallowMount } from "@vue/test-utils";
import ContactDetails from "../../src/components/ContactDetails.vue";
test("Comprobamos que información de App imprimimos en la pantalla", () => {
  //crear componente con las propiedades
  //comprobar que el nombre y el apellido estan en su sitio
  const wrapper = shallowMount(ContactDetails, {
    propsData: {
      contact: {
        name: "yassin",
        familyName: "ajami",
        mobile: "631125578",
        email: "ajami.my@gmail.com"
      },
      comments: null,
      idContact: null
    }
  });
  //buscamos la clase de la plantilla html (find)
  const nameTemplate = wrapper.find(".spanName");
  const surnameTemplate = wrapper.find(".spanFamilyName");
  const telTemplate = wrapper.find(".spanTel");
  const emailTemplate = wrapper.find(".spanEmail");
  const textName = nameTemplate.text();
  const textSurname = surnameTemplate.text();
  const textTel = telTemplate.text();
  const textEmail = emailTemplate.text();
  expect(textName).toBe("yassin");
  expect(textSurname).toBe("ajami");
  expect(textTel).toBe("631125578");
  expect(textEmail).toBe("ajami.my@gmail.com");
});
test("button edit starting event 'goToEdit'", async () => {
  const wrapper = shallowMount(ContactDetails, {
    propsData: {
      contact: []
    }
  });

  expect(wrapper.emitted().click).toBe(undefined);

  const buttonEdit = wrapper.find(".edit");
  buttonEdit.trigger("click");
  await wrapper.vm.$nextTick();
  expect(wrapper.emitted()["go-to-edit-contact-screene-vent"].length).toBe(1);
  expect(wrapper.emitted()["go-to-edit-contact-screene-vent"][0]).toEqual([]);
});
test("button cancel starting event 'goBack'", async () => {
  const wrapper = shallowMount(ContactDetails, {
    propsData: {
      contact: {}
    }
  });

  expect(wrapper.emitted().click).toBe(undefined);
  wrapper.vm.screenState = "contactDetailsScreen";
  const buttonCancel = wrapper.find(".cancel");
  buttonCancel.trigger("click");
  await wrapper.vm.$nextTick();
  expect(wrapper.emitted()["go-back-to-start-screen-even"].length).toBe(1);
  expect(wrapper.emitted()["go-back-to-start-screen-even"][0]).toEqual([]);
});

import { shallowMount } from "@vue/test-utils";
import App from "@/App.vue";
import ContactList from "@/components/ContactList.vue";
import AddContact from "@/components/AddContact.vue";
import EditContact from "@/components/EditContact.vue";

test("cuando recibe 'go-to-add-contact-event', cambia la pantalla ", async () => {
  const wrapper = shallowMount(App, {
    data() {
      return {
        screenState: "contactListScreen"
      };
    },
    methods: {
      ...App.methods,
      loadContactList: jest.fn()
    }
  });

  const contactList = wrapper.find(ContactList);

  expect(wrapper.vm.screenState).toBe("contactListScreen");

  contactList.vm.$emit("go-to-add-contact-event");

  expect(wrapper.vm.screenState).toBe("addContactScreen");
});

test("si funciona la function getContactList ", async () => {
  const MockApiCall = jest.fn();
  const mockContacts = [
    {
      id: "01",
      name: "yassin",
      familyName: "ajami",
      mobile: "631125578",
      email: "ajami.my@gmail.com"
    },
    {
      id: "02",
      name: "yeray",
      familyName: "espinosa",
      mobile: "63254789",
      email: "yeray.esp@gmail.com"
    }
  ];
  MockApiCall.mockReturnValue(mockContacts);

  const wrapper = shallowMount(App, {
    data() {
      return {
        contacts: null
      };
    },
    methods: {
      ...App.methods,
      loadContactList: MockApiCall
    }
  });

  await wrapper.vm.$nextTick();

  expect(MockApiCall).toHaveBeenCalled();

  expect(wrapper.vm.contacts).toEqual(mockContacts);
});

test("si funciona la function getContactDetails ", async () => {
  const MockApiCall = jest.fn();
  const mockContact = [
    {
      id: "01",
      name: "yassin",
      familyName: "ajami",
      mobile: "631125578",
      email: "ajami.my@gmail.com"
    }
  ];
  MockApiCall.mockReturnValue(mockContact);

  const wrapper = shallowMount(App, {
    data() {
      return {
        contact: null
      };
    },
    methods: {
      ...App.methods,
      loadContactDetails: MockApiCall
    }
  });

  const contactList = wrapper.find(ContactList);

  contactList.vm.$emit("see-contact-details-event");
  await wrapper.vm.$nextTick();

  expect(MockApiCall).toHaveBeenCalled();

  expect(wrapper.vm.contact).toEqual(mockContact);
});

test("comprobamos que deleteEvent borra el contacto", async () => {
  const MockApiCall = jest.fn();
  const mockContact = undefined;
  MockApiCall.mockReturnValue(mockContact);
  const wrapper = shallowMount(App, {
    data() {
      return {
        contact: {
          id: "01",
          name: "yassin",
          familyName: "ajami",
          mobile: "631125578",
          email: "ajami.my@gmail.com"
        }
      };
    },

    methods: {
      ...App.methods,
      deleteContact: MockApiCall,
      onLoadingPage: MockApiCall
    }
  });

  await wrapper.vm.$nextTick();

  expect(MockApiCall).toHaveBeenCalled();

  expect(wrapper.vm.contact.length).toBe(undefined);
});
test("comprobamos que deleteComment borra los comments", async () => {
  const MockApiCall = jest.fn();
  const mockComments = undefined;
  MockApiCall.mockReturnValue(mockComments);
  const wrapper = shallowMount(App, {
    data() {
      return {
        comments: {
          id: "111",
          idContact: "yassine",
          commentDate: "12/12/2020",
          commentDetails: "test test test "
        }
      };
    },
    methods: {
      ...App.methods,
      deleteComments: MockApiCall,
      onLoadingPage: MockApiCall
    }
  });
  //localhost:8080/
  await wrapper.vm.$nextTick();

  expect(MockApiCall).toHaveBeenCalled();

  expect(wrapper.vm.comments.length).toBe(undefined);
});

test("si funciona la function savecontactevent", async () => {
  const MockApiSendContact = jest.fn();
  const mockContact = [
    {
      id: "01",
      name: "yassin",
      familyName: "ajami",
      mobile: "631125578",
      email: "ajami.my@gmail.com"
    }
  ];
  MockApiSendContact.mockReturnValue(mockContact);

  const wrapper = shallowMount(App, {
    data() {
      return {};
    },
    methods: {
      ...App.methods,
      uploadNewContact: MockApiSendContact
    }
  });
  const xcontact = {
    id: "01",
    name: "yassin",
    familyName: "ajami",
    mobile: "631125578",
    email: "ajami.my@gmail.com"
  };
  console.log(wrapper.html());

  wrapper.vm.screenState = "addContactScreen";
  await wrapper.vm.$nextTick();

  const addContact = wrapper.find(AddContact);

  addContact.vm.$emit("save-contact-event", xcontact);

  await wrapper.vm.$nextTick();

  expect(MockApiSendContact).toHaveBeenCalled();
});

test("si funciona la function save localContact ", async () => {
  const MockApiSendEditContact = jest.fn();
  const mockContact = [
    {
      id: "01",
      name: "yassin",
      familyName: "ajami",
      mobile: "631125578",
      email: "ajami.my@gmail.com"
    }
  ];
  MockApiSendEditContact.mockReturnValue(mockContact);

  const wrapper = shallowMount(App, {
    data() {
      return {};
    },
    methods: {
      ...App.methods,
      saveContactChanges: MockApiSendEditContact
    }
  });
  const localContact = {
    id: "01",
    name: "yassin",
    familyName: "ajami",
    mobile: "631125578",
    email: "ajami.my@gmail.com"
  };

  wrapper.vm.screenState = "contactEditScreen";
  await wrapper.vm.$nextTick();
  const editContact = wrapper.find(EditContact);
  editContact.vm.$emit("save-contac-tevent", localContact);

  expect(MockApiSendEditContact).toHaveBeenCalled();
});

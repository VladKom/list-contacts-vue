import { shallowMount } from "@vue/test-utils";
import CommentList from "@/components/CommentList.vue";
import CommentCard from "@/components/CommentCard.vue";

test("pinta todo list vacío", () => {
  const wrapper = shallowMount(CommentList, {
    propsData: {
      detailsComment: []
    }
  });
  const commentCard = wrapper.findAll(CommentCard).wrappers;

  expect(commentCard.length).toBe(0);
});

test("pinta todo los commentos", () => {
  const detailsComment = [
    {
      id: "111",
      idContact: "yassine",
      commentDate: "12/12/2020",
      commentDetails: "test test test "
    },
    {
      id: "222",
      idContact: "vlad",
      commentDate: "12/12/2020",
      commentDetails: "test test test "
    }
  ];
  const wrapper = shallowMount(CommentList, {
    propsData: {
      listOfComment: detailsComment
    }
  });
  const commentCard = wrapper.findAll(CommentCard).wrappers;
  expect(commentCard.length).toBe(2);
});

import { shallowMount } from "@vue/test-utils";
import AddComments from "@/components/AddComments.vue";

test("emite click", async () => {
  const wrapper = shallowMount(AddComments, {
    propsData: {
      idContact: "00000000"
    },
    data() {
      return {
        comment: {
          idContact: "00000000",
          commentDate: null,
          commentDetails: null
        }
      };
    }
  });
  expect(wrapper.emitted().search).toBe(undefined);
  // Simular lo que hace el usuario.

  
  const input2 = wrapper.find("#comment-date");
  const input3 = wrapper.find("#new-comment");
  const submit = wrapper.find(".post");

  wrapper.vm.screenState = "AddCommentScreen";

  input2.setValue("21/11/2018");
  input3.setValue("Hemos estado reunidos");

  submit.trigger("click");

  await wrapper.vm.$nextTick();
  console.log(wrapper.emitted());
  expect(wrapper.emitted()["save-comment-event"].length).toBe(1);
  expect(wrapper.emitted()["save-comment-event"]).toEqual([
    [
      {
        idContact: "00000000",
        commentDate: "21/11/2018",
        commentDetails: "Hemos estado reunidos"
      }
    ]
  ]);
});
